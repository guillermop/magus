# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from gettext import gettext as _
from typing import Callable

from gi.repository import Adw, Gio, GLib, Gtk

from .window import Window
from .define import APP_ID, DEVEL, VERSION


class Application(Adw.Application):
    def __init__(self):
        self.settings = Gio.Settings.new(APP_ID)
        super().__init__(
            flags=Gio.ApplicationFlags.FLAGS_NONE,
            application_id=APP_ID,
            resource_base_path="/com/gitlab/guillermop/Magus",
        )

    def do_startup(self):
        Adw.Application.do_startup(self)
        GLib.set_application_name(_("Magus"))
        self._add_action("quit", self.on_quit)
        self._add_action("about", self.on_about)
        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])
        self.set_accels_for_action("win.add", ["<Ctrl>N"])
        self.set_accels_for_action("win.select-all", ["<Ctrl>A"])
        self.set_accels_for_action("win.show-help-overlay", ["<Ctrl>question"])

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = Window(application=self)
            if DEVEL:
                win.get_style_context().add_class("devel")
        win.present()

    def do_handle_local_options(self, options: GLib.VariantDict) -> int:
        parsed = options.end().unpack()

        loglevel = logging.ERROR
        if DEVEL or "debug" in parsed:
            loglevel = logging.DEBUG

        logging.basicConfig(
            format="[%(levelname)-s] %(asctime)s %(message)s", level=loglevel
        )
        return -1

    def _add_action(self, key: str, callback: Callable):
        action = Gio.SimpleAction(name=key)
        action.connect("activate", callback)
        super().add_action(action)

    def on_about(self, *args):
        about = Adw.AboutWindow()
        about.set_transient_for(self.props.active_window)
        about.set_application_name(_("Magus"))
        about.set_comments(_("A Wake-on-LAN application"))
        about.set_developer_name("Guillermo Peña")
        about.set_copyright("© 2022 Guillermo Peña")
        about.set_version(VERSION)
        about.set_issue_url("https://gitlab.com/guillermop/magus/-/issues/new")
        about.set_license_type(Gtk.License.GPL_3_0)
        about.set_website("https://gitlab.com/guillermop/magus")
        about.set_translator_credits(_("translator-credits"))
        about.present()

    def on_quit(self, *args):
        if self.props.active_window:
            self.props.active_window.close()
        self.quit()
