# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from typing import cast

from gi.repository import Adw, Gdk, GObject, Gtk

from .arp import get_arp_table
from .utils import is_valid_ip, is_valid_mac


@Gtk.Template(resource_path="/com/gitlab/guillermop/Magus/gtk/dialog.ui")
class Dialog(Adw.Window):
    __gtype_name__ = "Dialog"

    __gsignals__ = {"response": (GObject.SIGNAL_RUN_FIRST, None, (int,))}

    edit_mode = GObject.Property(type=bool, default=False)

    navigation_view = cast(Adw.NavigationView, Gtk.Template.Child())
    arp_stack = cast(Gtk.Stack, Gtk.Template.Child())
    name_entry = cast(Adw.EntryRow, Gtk.Template.Child())
    mac_entry = cast(Adw.EntryRow, Gtk.Template.Child())
    port_spin = cast(Adw.SpinRow, Gtk.Template.Child())
    broadcast_entry = cast(Adw.EntryRow, Gtk.Template.Child())
    save_button = cast(Gtk.Button, Gtk.Template.Child())
    list_box = cast(Gtk.ListBox, Gtk.Template.Child())

    def __init__(self, machine=None, **kwargs):
        super().__init__(**kwargs)
        self.machine = machine
        self._validators = {
            "name": lambda x: bool(x and x.strip()),
            "mac": is_valid_mac,
            "broadcast": is_valid_ip,
        }
        self._valid = {
            "name": False,
            "mac": False,
            "broadcast": True,
        }
        self._init_widgets()

    def _init_widgets(self):
        title = _("Add a New Machine")
        if self.machine:
            title = _("Edit Machine")
            self.edit_mode = True
            self.name_entry.set_text(self.machine.name)
            self.mac_entry.set_text(self.machine.mac)
            self.port_spin.set_value(self.machine.port)
            self.broadcast_entry.set_text(self.machine.broadcast)
        self.name_entry.grab_focus()
        self.set_title(title)
        cast(Gtk.Text, self.mac_entry.get_delegate()).set_max_length(17)
        cast(Gtk.Text, self.broadcast_entry.get_delegate()).set_max_length(15)

    @property
    def can_save(self):
        return all(self._valid.values())

    def get_data(self):
        return {
            "name": self.name_entry.get_text(),
            "mac": self.mac_entry.get_text().upper(),
            "port": int(self.port_spin.get_value()),
            "broadcast": self.broadcast_entry.get_text(),
        }

    @Gtk.Template.Callback()
    def on_cancel_button_clicked(self, *args):
        self.emit("response", Gtk.ResponseType.CANCEL)

    @Gtk.Template.Callback()
    def on_save_button_clicked(self, *args):
        if self.can_save:
            self.emit("response", Gtk.ResponseType.OK)

    @Gtk.Template.Callback()
    def on_scan_button_clicked(self, *args):
        self.navigation_view.push_by_tag("scanner")
        self.refresh()

    @Gtk.Template.Callback()
    def update_response_buttons(self, entry: Adw.EntryRow):
        buildable_id = entry.get_buildable_id()
        if not buildable_id:
            return
        field, _other = buildable_id.split("_")

        valid = self._validators[field](entry.get_text())

        if valid:
            entry.get_style_context().remove_class("error")
        else:
            entry.get_style_context().add_class("error")

        self._valid[field] = valid

        self.save_button.set_sensitive(self.can_save)

    def refresh(self):
        row = self.list_box.get_first_child()
        while row:
            temp = row.get_next_sibling()
            self.list_box.remove(row)
            row = temp
        visible_child = "empty"
        for hostname, mac in get_arp_table():
            action_row = Adw.ActionRow(
                title=hostname,
                subtitle=mac,
                activatable=True,
                selectable=False,
            )
            action_row.add_suffix(Gtk.Image(icon_name="go-next-symbolic"))
            self.list_box.append(action_row)
            visible_child = "list"
        self.arp_stack.set_visible_child_name(visible_child)

    @Gtk.Template.Callback()
    def on_refresh_button_clicked(self, *args):
        self.refresh()

    @Gtk.Template.Callback()
    def on_key_press_event(self, _event, keyval: int, *args):
        if keyval == Gdk.KEY_Escape:
            self.emit("response", Gtk.ResponseType.CANCEL)
            return True
        return False

    @Gtk.Template.Callback()
    def on_row_activated(self, list_box, row: Adw.ActionRow):
        self.name_entry.set_text(row.get_title())
        self.mac_entry.set_text(row.get_subtitle() or "")
        self.navigation_view.pop()
        self.set_default_widget(self.save_button)
        self.name_entry.grab_focus()
