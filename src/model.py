# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import TypedDict

from gi.repository import Gio, GObject

from .database import Database
from .machine import Machine


class TrashItem(TypedDict):
    position: int
    item: Machine


class Model(Gio.ListStore):
    empty = GObject.Property(type=bool, default=True)

    def __init__(self) -> None:
        super().__init__(item_type=Machine)
        self.connect("items-changed", self._on_items_changed)
        self._trash: list[TrashItem] = []

    def load(self):
        self.splice(0, 0, Database.get_default().machines)

    def create(self, **kwargs):
        machine = Database.get_default().insert_machine(**kwargs)
        if machine:
            self.append(machine)
        return machine

    def _on_items_changed(self, *args):
        self.empty = self.get_n_items() == 0

    def delete_item(self, item: Machine):
        found, position = self.find(item)
        if not found:
            return
        self._trash.append({"position": position, "item": item})
        super().remove(position)

    def restore_trash(self):
        for item in reversed(self._trash):
            self.insert(item["position"], item["item"])
        self._trash = []

    def empty_trash(self):
        for item in self._trash:
            item["item"].delete()
        self._trash = []

    @property
    def trash_n_items(self):
        return len(self._trash)
