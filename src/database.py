# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import sqlite3
from os import makedirs, path
from typing import List, Optional

from gi.repository import GLib

from .machine import Machine


class Database:
    instance: Optional["Database"] = None

    def __init__(self):
        filename = path.join(GLib.get_user_data_dir(), "magus", "database.db")
        makedirs(path.dirname(filename), exist_ok=True)
        self.__connection = sqlite3.connect(filename)
        self.__connection.row_factory = sqlite3.Row
        self.__create_tables()

    @staticmethod
    def get_default() -> "Database":
        if Database.instance is None:
            Database.instance = Database()
        return Database.instance

    @property
    def machines(self) -> List[Machine]:
        query = "SELECT * FROM machines"
        try:
            data = self.__connection.cursor().execute(query)
            rows = data.fetchall()
            return [Machine(**machine) for machine in rows]
        except sqlite3.OperationalError as error:
            logging.error(error)
        return []

    def insert_machine(self, **kwargs) -> Optional[Machine]:
        query = "INSERT INTO machines (id,"
        query += ",".join(kwargs.keys())
        query += ") VALUES (null, ?, ?, ?, ?)"

        cursor = self.__connection.cursor()
        try:
            cursor.execute(query, [*kwargs.values()])
            self.__connection.commit()
            return Machine(id=cursor.lastrowid, **kwargs)
        except sqlite3.OperationalError as error:
            logging.error(error)
        return None

    def delete_machine_by_id(self, id_: int):
        query = "DELETE FROM machines WHERE id=?"
        try:
            self.__connection.execute(query, [id_])
            self.__connection.commit()
        except sqlite3.Error as error:
            logging.error(error)

    def update_machine_by_id(self, id_: int, **kwargs):
        query = "UPDATE machines SET "
        query += ", ".join([f"{column}=?" for column in kwargs])
        query += " WHERE id=?"
        try:
            self.__connection.execute(query, list(kwargs.values()) + [id_])
            self.__connection.commit()
        except sqlite3.OperationalError as error:
            logging.error(error)

    def __create_tables(self):
        cursor = self.__connection.cursor()
        cursor.executescript(
            """
                CREATE TABLE IF NOT EXISTS "machines" (
                    "id"	INTEGER NOT NULL UNIQUE,
                    "name"	TEXT NOT NULL,
                    "mac"	TEXT NOT NULL,
                    "port"	INTEGER NOT NULL,
                    "broadcast"	TEXT NOT NULL,
                    PRIMARY KEY("id" AUTOINCREMENT)
                );
            """
        )
