# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import cast

from gi.repository import Gtk, Gio, Adw, GObject

from .list_box import ListBox


@Gtk.Template(resource_path="/com/gitlab/guillermop/Magus/gtk/row.ui")
class Row(Adw.ActionRow):
    __gtype_name__ = "Row"

    selection_mode = GObject.Property(type=bool, default=False)
    selected = GObject.Property(type=bool, default=False)

    wake_button = cast(Gtk.Button, Gtk.Template.Child())

    def __init__(self, machine):
        super().__init__(visible=True, activatable=False)
        self._action_group = Gio.SimpleActionGroup()
        self.insert_action_group("row", self._action_group)
        self.machine = machine
        self.bind_properties()
        self._setup_actions()

    def bind_properties(self):
        self.machine.bind_property(
            "name", self, "title", GObject.BindingFlags.SYNC_CREATE
        )
        self.machine.bind_property(
            "mac", self, "subtitle", GObject.BindingFlags.SYNC_CREATE
        )

    def _setup_actions(self):
        self._add_action("wake", self.wake)
        self._add_action("edit", self.edit)

    def _add_action(self, key, callback, bind=False):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        self._action_group.add_action(action)

    def wake(self, *args):
        from .window import Window

        root = cast(Window, self.get_root())
        root.wakeup_machines(self.machine)

    def edit(self, *args):
        from .window import Window

        root = cast(Window, self.get_root())
        root.edit_machine(self.machine)

    @Gtk.Template.Callback()
    def on_selected_changed(self, *args):
        listbox = cast(ListBox, self.get_parent())
        if not listbox:
            return
        if self.selected:
            if not self.is_selected():
                listbox.select_row(self)
        else:
            listbox.unselect_row(self)

    @Gtk.Template.Callback()
    def on_selection_mode_changed(self, *args):
        self.set_activatable(self.selection_mode)
        self.set_selectable(self.selection_mode)
        self.selected = False
