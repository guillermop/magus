# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import ipaddress
import re

MAC_PATTERN = "^([\\dA-F]{2}:){5}([\\dA-F]{2})$"


def is_valid_mac(mac: str):
    if not mac:
        return False
    return bool(re.match(MAC_PATTERN, mac.upper()))


def is_valid_ip(ip_addr: str):
    if not ip_addr:
        return False
    try:
        ipaddress.ip_address(ip_addr)
    except ValueError:
        return False
    return True
