# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import socket

ARP_PATH = "/proc/net/arp"


def get_arp_table():
    if not os.path.exists(ARP_PATH):
        return
    with open(ARP_PATH, encoding="utf-8", mode="r") as file:
        file.readline()
        line = file.readline()
        while line:
            columns = line.split()
            ip_addr, mac = columns[0], columns[3]
            if mac != "00:00:00:00:00:00":
                yield socket.getfqdn(ip_addr) or ip_addr, mac.upper()
            line = file.readline()
