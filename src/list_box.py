# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import cast

from gi.repository import Gdk, GObject, Gtk


class ListBox(Gtk.ListBox):
    __gtype_name__ = "ListBox"

    has_selection = GObject.Property(type=bool, default=False)

    def __init__(self):
        super().__init__()
        controller = Gtk.GestureClick()
        controller.connect("released", self.on_button_release_event)
        self.add_controller(controller)
        self.connect_after("selected-rows-changed", self.on_selected_rows_changed)

    def on_button_release_event(self, gesture: Gtk.Gesture, n: int, x: int, y: int):
        from .window import Window

        window = cast(Window, self.get_root())

        state = gesture.get_current_event_state()

        if not window.selection_mode:
            if (
                state & Gdk.ModifierType.CONTROL_MASK
                or state & Gdk.ModifierType.SHIFT_MASK
            ):
                window.selection_mode = True

        row = self.get_row_at_y(y)
        if not row:
            return
        if row.is_selected():
            self.unselect_row(row)
        else:
            self.select_row(row)
        gesture.set_state(Gtk.EventSequenceState.CLAIMED)

    def on_selected_rows_changed(self, *args):
        has_selection = False
        for row in self:  # type: ignore
            row.props.selected = row.is_selected()
            has_selection = row.props.selected or has_selection

        self.has_selection = has_selection
