# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

import socket


def send_magic_packet(*machines):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    for machine in machines:
        address = (machine.broadcast, machine.port)
        payload = bytes.fromhex(f'FFFFFFFFFFFF{machine.mac.replace(":", "") * 16}')
        sock.sendto(payload, address)
