# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject


class Machine(GObject.Object):
    def __init__(self, **kwargs):
        super().__init__()
        self._name = kwargs.get("name")
        self._mac = kwargs.get("mac")
        self._id = kwargs.get("id")
        self._port = kwargs.get("port")
        self._broadcast = kwargs.get("broadcast")

    @GObject.Property(type=str)
    def name(self):  # type: ignore
        return self._name

    @name.setter
    def name(self, name: str):
        self._name = name

    @GObject.Property(type=str)
    def mac(self):  # type: ignore
        return self._mac

    @mac.setter
    def mac(self, mac):
        self._mac = mac

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port = port

    @property
    def broadcast(self):
        return self._broadcast

    @broadcast.setter
    def broadcast(self, broadcast):
        self._broadcast = broadcast

    def save(self):
        from .database import Database

        Database.get_default().update_machine_by_id(
            self._id,  # type: ignore
            name=self._name,
            mac=self._mac,
            port=self._port,
            broadcast=self._broadcast,
        )

    def delete(self):
        from .database import Database

        Database.get_default().delete_machine_by_id(self._id)  # type: ignore
