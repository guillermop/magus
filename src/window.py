# SPDX-FileCopyrightText: 2023 Guillermo Peña
# SPDX-License-Identifier: GPL-3.0-or-later

from gettext import gettext as _
from gettext import ngettext
from typing import Optional, cast

from gi.repository import Adw, Gio, GObject, Gtk

from .database import Machine
from .dialog import Dialog
from .list_box import ListBox
from .model import Model
from .row import Row
from .wol import send_magic_packet


@Gtk.Template(resource_path="/com/gitlab/guillermop/Magus/gtk/window.ui")
class Window(Adw.ApplicationWindow):
    __gtype_name__ = "Window"

    selection_mode = GObject.Property(type=bool, default=False)

    header_bar = cast(Adw.HeaderBar, Gtk.Template.Child())
    header_stack = cast(Gtk.Stack, Gtk.Template.Child())
    main_stack = cast(Gtk.Stack, Gtk.Template.Child())
    list_box = cast(ListBox, Gtk.Template.Child())
    overlay = cast(Adw.ToastOverlay, Gtk.Template.Child())
    selection_button = cast(Gtk.MenuButton, Gtk.Template.Child())
    select_button = cast(Gtk.ToggleButton, Gtk.Template.Child())
    cancel_button = cast(Gtk.ToggleButton, Gtk.Template.Child())
    toast: Optional[Adw.Toast] = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.model = Model()
        self.model.connect("notify::empty", self.on_empty_changed)
        self.model.load()
        self._bind_properties()
        self._setup_actions()
        self._restore_window_state()

    def _restore_window_state(self):
        from .application import Application

        settings = cast(Application, self.props.application).settings
        width = settings.get_int("window-width")
        height = settings.get_int("window-height")
        maximized = settings.get_boolean("window-maximized")
        self.set_default_size(width, height)
        if maximized:
            self.maximize()

    def _bind_properties(self):
        self.list_box.bind_model(self.model, self._create_row_func)
        self.model.bind_property(
            "empty",
            self.select_button,
            "sensitive",
            GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN,
        )

    def _create_row_func(self, *args):
        row = Row(*args)
        self.bind_property(
            "selection-mode", row, "selection-mode", GObject.BindingFlags.SYNC_CREATE
        )
        return row

    def _setup_actions(self):
        self._add_action("add", lambda *_: self.show_machine_dialog())
        self._add_action("select-all", self.select_all, True)
        self._add_action("select-none", self.select_none, True)

    def _add_action(self, key, callback, bind=False):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        if bind:
            self.model.bind_property(
                "empty",
                action,
                "enabled",
                GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE,
            )
        super().add_action(action)

    def wakeup_machines(self, *machines: Machine):
        send_magic_packet(*machines)
        message = _("Waking up {0} machines.").format(len(machines))
        if len(machines) == 1:
            message = _('Waking up "{0}".').format(machines[0].name)
        self.overlay.add_toast(Adw.Toast(title=message))

    def delete_machines(self, *machines: Machine):
        for machine in machines:
            self.model.delete_item(machine)

        if self.model.trash_n_items > 1:
            message = _("{0} machines deleted").format(self.model.trash_n_items)
        else:
            message = _('Machine "{0}" deleted.').format(machines[0].name)

        if self.toast:
            self.toast.set_title(message)
            return

        self.toast = Adw.Toast(
            title=message, button_label=_("Undo"), priority=Adw.ToastPriority.HIGH
        )

        def on_dismissed(*args):
            self.toast = None
            self.model.empty_trash()

        def on_undo(*args):
            self.model.restore_trash()

        self.toast.connect("dismissed", on_dismissed)
        self.toast.connect("button-clicked", on_undo)
        self.overlay.add_toast(self.toast)

    def edit_machine(self, machine: Machine):
        self.show_machine_dialog(machine)

    def select_all(self, *args):
        if not self.selection_mode:
            self.selection_mode = True
        self.list_box.select_all()

    def select_none(self, *args):
        self.list_box.unselect_all()

    def on_empty_changed(self, *args):
        self.selection_mode = False
        self.main_stack.set_visible_child_name(
            "empty" if self.model.empty else "default"
        )

    @Gtk.Template.Callback()
    def on_wake_button_clicked(self, *args):
        self.wakeup_machines(
            *[cast(Row, row).machine for row in self.list_box.get_selected_rows()]
        )

    @Gtk.Template.Callback()
    def on_delete_button_clicked(self, *args):
        self.delete_machines(
            *[cast(Row, row).machine for row in self.list_box.get_selected_rows()]
        )

    @Gtk.Template.Callback()
    def on_cancel_button_clicked(self, *args):
        self.selection_mode = False

    @Gtk.Template.Callback()
    def on_selection_mode_changed(self, *args):
        self.list_box.unselect_all()
        if self.selection_mode:
            self.header_stack.set_visible_child_name("selection")
            self.cancel_button.grab_focus()
            self.header_bar.set_decoration_layout("None")
        else:
            self.header_stack.set_visible_child_name("default")
            self.select_button.grab_focus()
            self.header_bar.set_decoration_layout(None)

    @Gtk.Template.Callback()
    def on_selected_rows_changed(self, *args):
        selected = len(self.list_box.get_selected_rows())
        if selected == 0:
            text = _("Click on items to select them")
        else:
            text = ngettext(
                "{} selected machine", "{} selected machines", selected
            ).format(selected)

        self.selection_button.set_label(text)

    @Gtk.Template.Callback()
    def on_close_request(self, *args):
        from .application import Application

        settings = cast(Application, self.props.application).settings
        width, height = self.get_default_size()
        settings.set_int("window-width", width)
        settings.set_int("window-height", height)
        settings.set_boolean("window-maximized", self.is_maximized())

    def show_machine_dialog(self, machine: Optional[Machine] = None):
        dialog = Dialog(machine, transient_for=self)

        def on_response(_dialog, response):
            if response == Gtk.ResponseType.OK:
                data = dialog.get_data()
                if machine:
                    for key, value in data.items():
                        setattr(machine, key, value)
                    machine.save()
                else:
                    self.model.create(**data)

            dialog.destroy()

        dialog.connect("response", on_response)
        dialog.present()
